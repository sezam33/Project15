﻿using UnityEngine;

public class Tile : MonoBehaviour
{
	private const float SIZE = 100f;
	private float moveSpeed = 500f;

	public int space { get; set; }
	public int index { get; set; }

	private bool isMoving;
	private Vector2 destination;
	private RectTransform rectTr;

	private System.Action<int, int> endMoveCallback;

	private void Awake()
	{
		rectTr = GetComponent<RectTransform>();
		index = int.Parse(gameObject.name);
	}

	private void Update()
	{
		if (isMoving)
		{
			rectTr.anchoredPosition = Vector2.MoveTowards(rectTr.anchoredPosition, destination, Time.deltaTime * moveSpeed);
			if (rectTr.anchoredPosition == destination)
				MoveEnded();
		}
	}

	public void Move(int newSpace, System.Action<int, int> callback)
	{
		space = newSpace;
		destination = SIZE * Field.GetSpacePosition(space);
		endMoveCallback = callback;
		isMoving = true;
	}

	private void MoveEnded()
	{
		if (endMoveCallback != null) endMoveCallback(index, space);
		isMoving = false;
	}

	public void SetSpace(int newSpace)
	{
		space = newSpace;
		Vector2 position = Field.GetSpacePosition(space) * SIZE;
		rectTr.anchoredPosition = position;
	}
}
