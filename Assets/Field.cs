﻿using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
	public const int SIZE = 5;
	[SerializeField]
	private Tile[] tiles;
	[SerializeField]
	private GameObject winPanel;
	private int[] spaces = new int[SIZE * SIZE];
	private int emptySpace;
	private int movingTilesAmount;
	private bool isGameEnded;

	private void Start()
	{
		OnNewGamePressed();
	}

	public void OnTilePressed(GameObject go)
	{
		if (movingTilesAmount > 0 || isGameEnded)
			return;

		int tileIndex = int.Parse(go.name);
		int tileSpace = tiles[tileIndex].space;

		bool sameRow = (tileSpace / SIZE == emptySpace / SIZE);
		bool sameColumn = (tileSpace % SIZE == emptySpace % SIZE);
		if (!sameRow && !sameColumn) return;

		int step = (sameColumn ? SIZE : 1);
        int min = tileSpace;
        int max = emptySpace;
        int dir = 1;
        if (emptySpace < tileSpace)
        {
            min = emptySpace + step;
            max = tileSpace + step;
            dir = -1;
        }
        for (int i = min; i < max; i += step)
        {
            movingTilesAmount++;
            tiles[spaces[i]].Move(i + dir * step, OnTileStopMoving);
        }

        emptySpace = tileSpace;
		spaces[emptySpace] = -1;
	}

	public static Vector2 GetSpacePosition(int space)
	{
		return new Vector2(space % Field.SIZE, -1 * space / Field.SIZE);
	}

	public void OnTileStopMoving(int tileIndex, int tileNewSpace)
	{
		spaces[tileNewSpace] = tileIndex;
		movingTilesAmount--;
		if (movingTilesAmount == 0) CheckForVictory();
	}

	private void CheckForVictory()
	{
		isGameEnded = true;
		for (int i = 0; i < tiles.Length; i++)
		{
			if (tiles[i].space != i)
			{
				isGameEnded = false;
				return;
			}
		}
		winPanel.SetActive(true);
	}

	public void OnNewGamePressed()
	{
		List<int> indexes = new List<int>();
		for (int i = 0; i < spaces.Length; i++)
		{
			indexes.Add(i);
		}
		for (int i = 0; i < tiles.Length; i++)
		{
			int randomIndex = Random.Range(0, indexes.Count);
			spaces[indexes[randomIndex]] = tiles[i].index;
			tiles[i].SetSpace(indexes[randomIndex]);
			indexes.RemoveAt(randomIndex);
		}
		spaces[indexes[0]] = -1;
		emptySpace = indexes[0];
		movingTilesAmount = 0;
		isGameEnded = false;
	}

	public void OnQuitPressed()
	{
		Application.Quit();
	}
}
